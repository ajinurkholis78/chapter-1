import java.util.Arrays;

public class TugasLooping {
    public static void main(String[] args) {
        //Dengan menggunakan semua metode looping, buatlah pola-pola berikut!
        //2 4 8 16 32
        //2 4 8 32 256

        for (int i=2; i<=32; i=i*2){
            System.out.print(i);

        }System.out.println("\n");

        for (int i=2; i<=256; i= i*2){
            if (i == 16 || i == 24 || i==64 || i==128)
                continue;
            System.out.print(i);
        }System.out.println("\n");

        int[] array={6,6,5,9,2};
        Arrays.sort(array);
        for (int index:array){
            System.out.print(index);
        }System.out.println("\n");

        String [] aji={"M","A","K","A","N","N","A","S","I"};
        Arrays.sort(aji);
        for (String index:aji){
            System.out.print(index);
        }
    }
}
